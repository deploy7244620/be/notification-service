import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiscoveryDecoratorModule } from 'common/modules/discovery-decorator/discovery-decorator';
import { HealthModule } from 'common/modules/health/module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AddDefaultInterceptor } from 'common/interceptor/interceptor.add-default';
import { RedisModule } from '@nestjs-modules/ioredis';
import { AuthenMiddleware } from 'common/middleware/authentication';

import { MessagesModule } from './modules/messages/module';
import { AppGatewayModule } from './modules/gateway/module';

// let sentinelsHost = process.env.REDIS_SENTINEL_HOSTS.split(',') || [];
// const sentinelsPort = Number(process.env.REDIS_SENTINEL_PORT);
// const sentinels = sentinelsHost.map((item) => {
//   return { host: item, port: sentinelsPort };
// });
@Module({
  imports: [
    MessagesModule,
    HealthModule,
    DiscoveryDecoratorModule,
    AppGatewayModule,
    // TypeOrmModule.forRoot({
    //   type: 'postgres',
    //   name: 'postgres',
    //   host: process.env.POSTGRES_HOST,
    //   port: Number(process.env.POSTGRES_PORT),
    //   username: process.env.POSTGRES_USERNAME,
    //   password: process.env.POSTGRES_PASSWORD,
    //   database: process.env.POSTGRES_DATABASE,
    //   entities: [],
    //   synchronize: true,
    //   autoLoadEntities: true,
    // }),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      name: 'mongodb',
      url: process.env.MONGODB_DATABASE,
      entities: [],
      synchronize: true,
      autoLoadEntities: true,
    }),
    // RedisModule.forRoot(
    //   {
    //     config: {
    //       sentinels: sentinels,
    //       name: `${process.env.REDIS_SENTINEL_NAME}`,
    //       sentinelPassword: `${process.env.REDIS_SENTINEL_PASS}`,
    //       password: `${process.env.REDIS_PASS}`,
    //       db: Number(`${process.env.REDIS_DB}`),
    //     },
    //   },
    //   'redisW',
    // ),
    // RedisModule.forRoot(
    //   {
    //     config: {
    //       sentinels: sentinels,
    //       role: 'slave',
    //       name: `${process.env.REDIS_SENTINEL_NAME}`,
    //       sentinelPassword: `${process.env.REDIS_SENTINEL_PASS}`,
    //       password: `${process.env.REDIS_PASS}`,
    //       db: Number(`${process.env.REDIS_DB}`),
    //     },
    //   },
    //   'redisR',
    // ),
  ],
  controllers: [],
  // providers: [
  //   {
  //     provide: APP_INTERCEPTOR,
  //     useClass: AddDefaultInterceptor,
  //   },
  // ],
})
// export class AppModule implements NestModule {
//   configure(consumer: MiddlewareConsumer) {
//     consumer
//       .apply(AuthenMiddleware)
//       .exclude({ path: '/health', method: RequestMethod.ALL })
//       .forRoutes('*');
//   }
// }
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply()
      .exclude({ path: '/health', method: RequestMethod.ALL })
      .forRoutes('*');
  }
}
