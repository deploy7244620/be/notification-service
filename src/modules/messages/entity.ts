import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  to: string;
  from: string;
  message: string;
  messageStatus: string;
  sendDoneAt: Date;
  service: string;
}
@Entity({ name: 'messages' })
// @Index(['restaurantId', 'displayOrder'], { unique: true })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  sendDoneAt: Date;

  @Column({ nullable: true })
  to: string;

  @Column({ nullable: true })
  from: string;
  @Column({ nullable: true })
  message: string;

  @Column({ nullable: true })
  messageStatus: string;

  @Column({ nullable: true })
  service: string;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;
}
