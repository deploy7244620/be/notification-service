import { ApiProperty } from '@nestjs/swagger';
import { IsEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateDto {
  @IsString()
  @ApiProperty({ nullable: true })
  to: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  from: string;

  @IsString()
  @ApiProperty({ nullable: true })
  message: string;

  @IsOptional()
  @IsEmpty()
  @ApiProperty({ nullable: true })
  messageStatus: string;

  @IsOptional()
  @IsEmpty()
  @ApiProperty({ nullable: true })
  sendDoneAt: Date;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  service: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
