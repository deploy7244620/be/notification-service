import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { isJSON } from 'class-validator';
import { BaseGetListDto } from 'common/interface/repository/interface.base.dto';

export class GetListDto implements BaseGetListDto {
  @Transform(({ value }) => Number(value))
  @ApiProperty({ required: false, type: 'number' })
  page: number;

  @Transform(({ value }) => Number(value))
  @ApiProperty({ required: false, type: 'number' })
  size: number;

  @Transform(({ value }) => {
    return isJSON(value) ? JSON.parse(value) : value;
  })
  @ApiProperty({ required: false, type: 'object' })
  updateAtTimestamp: object;

  @Transform(({ value }) => {
    return isJSON(value) ? JSON.parse(value) : value;
  })
  @ApiProperty({ required: false, type: 'object' })
  updateAt: object;

  @Transform(({ value }) => {
    return isJSON(value) ? JSON.parse(value) : value;
  })
  @ApiProperty({ required: false, type: 'object' })
  createAtTimestamp: object;

  @Transform(({ value }) => {
    return isJSON(value) ? JSON.parse(value) : value;
  })
  @ApiProperty({ required: false, type: 'object' })
  createAt: object;

  @Transform(({ value }) => JSON.parse(value))
  @ApiProperty({ required: false, type: 'object' })
  orderBy: object;
}
