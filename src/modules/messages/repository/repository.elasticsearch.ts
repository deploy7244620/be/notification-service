import { Injectable } from '@nestjs/common';
import { AbstractRepositoryElasticsearch } from 'common/interface/repository/abstract.repository.elasticsearch';
import { IBaseRepositoryElasticsearch } from 'common/interface/repository/interface.repository.elasticsearch';
import { client } from '../../../../common/libs/elasticsearch.connect';

@Injectable()
export class RepositoryElasticSearch
  extends AbstractRepositoryElasticsearch
  implements IBaseRepositoryElasticsearch
{
  constructor(public nameIndex, public mapping) {
    // sua lai TaskEntity thanh Postgres
    super(client, nameIndex, mapping);
  }
}
