import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateDto } from './dto/create.dto';
import { EditDto } from './dto/edit.dto';
import { GetListDto } from './dto/get-list.dto';
import { Service } from './service';
import { MessageBus } from 'common/libs/message-bus';
import { Subscribe } from 'common/decorators/custom-decorators';
const messageBus = new MessageBus();
const rootFolder = __dirname.split('/').pop();

@Controller(rootFolder.toLocaleLowerCase())
@ApiTags(rootFolder)
export class ModuleController {
  constructor(private _service: Service) {}

  @Get('/search')
  async search(@Query() input) {
    return await this._service.search(input);
  }

  @Post()
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async add(@Body() body: CreateDto) {
    body.messageStatus = 'NOT';
    const data = await this._service.add(body);
    await messageBus.sendToQueue(
      data,
      'DOMAIN_EXCHANGE_DEV',
      'GATEWAY_MESSAGE',
    );
    return data;
  }

  @Subscribe('MESSAGE_DONE')
  async updateStatusMessage(input) {
    const body = {
      sendDoneAt: new Date(),
      messageStatus: 'DONE',
    };
    return await this._service.update(input.messageId, body);
  }

  @Delete('/:id')
  async delete(@Param('id') p_id) {
    return await this._service.delete(p_id);
  }

  @Get()
  async getList(@Query() input: GetListDto) {
    const data = await this._service.select(input);

    return data;
  }

  @Get('/:id')
  async get(@Param('id') p_id) {
    const data = await this._service.select({ id: p_id });

    return data;
  }

  @Put('/:id')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async edit(@Param('id') p_id, @Body() body: EditDto) {
    return await this._service.update(p_id, body);
  }
}
