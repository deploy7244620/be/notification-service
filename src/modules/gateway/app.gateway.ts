import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { Subscribe } from 'common/decorators/custom-decorators';
import { Service } from './service';
import * as os from 'os';
const HOST_NAME = os.hostname();
console.log(HOST_NAME);

@WebSocketGateway(3080, {
  cors: {
    origin: '*',
  },
})
export class AppGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer() server: Server;

  constructor(private _service: Service) {}

  @SubscribeMessage('push')
  async handleSendMessage(client: Socket, payload: any): Promise<void> {
    this.server.emit('recMap', payload);
  }

  async sendMessage(socketId, message) {
    await this.server.emit(socketId, message);
  }

  afterInit(server: Server) {
    console.log(`afterInit: ${server}`);
    //Do stuffs
  }

  async handleDisconnect(client: Socket) {
    console.log(`Disconnected: ${client.id}`);
    //Do stuffs
    await this._service.findOneAndUpdate(
      { socketId: client.id },
      {
        socketStatus: 'INACTIVE',
        socketDisconnectAt: new Date(),
        socketDisconnectAtTimestamp: new Date().getTime(),
      },
    );
  }

  async handleConnection(client: Socket, ...args: any[]) {
    console.log(`Connected ${client.id}`);
    const userId = client.handshake.headers['x-user-id'];
    const data = await this._service.add({
      ownerId: userId, // test
      socketId: client.id,
      hostId: HOST_NAME,
      socketStatus: 'ACTIVE',
      socketConnectAt: new Date(),
      socketConnectAtTimestamp: new Date().getTime(),
    });
    this.server.emit(`hello-${userId}`, `Connected ${client.id}`);
  }
}
