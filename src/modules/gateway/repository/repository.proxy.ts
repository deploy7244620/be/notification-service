import { Injectable } from '@nestjs/common';
import { AProxy } from 'common/interface/repository/abstract.repository.proxy.base';
@Injectable()
export class RepositoryProxy<Entity> extends AProxy<Entity> {
  constructor(public nameModule, public storage, public storageSearch?) {
    super(nameModule, storage, storageSearch);
  }
}
