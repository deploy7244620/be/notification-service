import { Controller } from '@nestjs/common';
import { Subscribe } from 'common/decorators/custom-decorators';
import { MessageBus } from 'common/libs/message-bus';
import * as os from 'os';
import { AppGateway } from './app.gateway';
import { Service } from './service';
const rootFolder = __dirname.split('/').pop();
const HOST_NAME = os.hostname();
console.log(HOST_NAME);
const messageBus = new MessageBus();

@Controller(rootFolder.toLocaleLowerCase())
export class ModuleController {
  constructor(private appGateway: AppGateway, private _service: Service) {}

  @Subscribe(HOST_NAME)
  async sendMessage(data) {
    await this.appGateway.sendMessage(data.socketId, data);
    //TODO check send thành công
    await messageBus.sendToQueue(
      {
        messageId: data._id,
      },
      'DOMAIN_EXCHANGE_DEV',
      'MESSAGE_DONE',
    );
  }

  @Subscribe('GATEWAY_MESSAGE')
  async handleMessage(input) {
    const ownerId = input.to;
    const data = await this._service.select({
      ownerId,
      socketStatus: 'ACTIVE',
    });

    if (data.count > 0) {
      const promises = data.data.map((item) => {
        const hostId = item.hostId;
        const socketId = item.socketId;
        input.socketId = socketId;

        return Promise.all([
          messageBus.sendToQueue(input, 'DOMAIN_EXCHANGE_DEV', hostId),
        ]);
      });

      await Promise.all(promises)
        .then((results) => {
          return results;
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
    // this.appGateway.sendMessage(ownerId, message);
  }
}
