import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';

export interface IEntity {
  ownerId: string;
  socketId: string;
  hostId: string;
  socketStatus: string;
  socketConnectAt: Date;
  socketConnectAtTimestamp: number;
  socketDisconnectAt: Date;
  socketDisconnectAtTimestamp: number;
}
@Entity({ name: 'socket-history' })
@Index(['socketId', 'hostId'])
@Index(['socketId'])
@Index(['ownerId'])
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  socketConnectAt: Date;

  @Column({ nullable: true })
  socketConnectAtTimestamp: number;

  @Column({ nullable: true })
  socketDisconnectAt: Date;

  @Column({ nullable: true })
  socketDisconnectAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  ownerId: string;

  @Column({ nullable: true })
  socketId: string;

  @Column({ nullable: true })
  hostId: string;

  @Column({ nullable: true })
  socketStatus: string;
}
