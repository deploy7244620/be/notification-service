import { Module } from '@nestjs/common';
import { AppGateway } from './app.gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntityMongodb } from './entity';
import { ModuleController } from './controller';
import { RepositoryMongodb } from './repository/repository.mongodb';
import { Service } from './service';

@Module({
  imports: [TypeOrmModule.forFeature([EntityMongodb], 'mongodb')],
  controllers: [ModuleController],
  providers: [
    AppGateway,
    Service,
    {
      provide: 'repoMongodb',
      useClass: RepositoryMongodb,
    },
  ],
})
export class AppGatewayModule {}
