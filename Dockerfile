# Base image
FROM node:19-alpine
WORKDIR /app
COPY . .
RUN npm install -f
RUN npm run build
ENV TZ=Asia/Ho_Chi_Minh
ENTRYPOINT npm run start:dev
EXPOSE 3000
EXPOSE 3080

