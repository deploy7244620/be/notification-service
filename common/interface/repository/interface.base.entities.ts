import {
  Column,
  Entity,
  Index,
  ObjectIdColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
export interface IBaseEntity {
  createAt: Date;
  createAtTimestamp: number;
  // createBy: string;
}
